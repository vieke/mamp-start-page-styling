Point MAMP in its settings to load index-of.php instead of its normal page when starting.

You can also edit the httpd.conf file:
<IfModule dir_module> 
    DirectoryIndex index-of.php index.html index.php 
</IfModule>  