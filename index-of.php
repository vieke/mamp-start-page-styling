<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>Index of MAMP</title>
<link href="index-of.php-sources/default.css" type="text/css" rel="stylesheet" />
</head>
<body>
<h1>MAMP Listing</h1>
<div class="items">
<?php
$dir = '.';
$directories = array();
$files_list  = array();
$files = scandir($dir);
natcasesort($files); //Sort alfabeticly even if the cases are not the same (E.g.: A,b,C,d,e,F...)
foreach($files as $file){
	$ignore = array(".DS_Store",".","..",".git",".gitignore","index-of.php-sources");
   if(!in_array($file, $ignore)){
      if(is_dir($dir.'/'.$file)){
         $directories[]  = $file;}//end if
	  else{$files_list[]    = $file;}//end else
   }//end if
}//end foreach
foreach($directories as $directory){echo '<p class="item folder"><a href="../'.$directory.'"><span>'.$directory.'</span></a></p>';}
foreach($files_list as $file_list){$extension = substr($file_list, -3);echo '<p class="item '.$extension.'"><a href="../'.$file_list.'"><span>'.$file_list.'</span></a></p>';}
?>
<div class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
		<li><a target="_blank" href="/MAMP/index.php?language=English">MAMP Start</a></li>
        <li>|</li>
		<li><a target="_blank" href="/MAMP/index.php?language=English&amp;page=phpinfo">phpInfo</a></li>
        <li>|</li>
		<li><a target="_blank" href="/MAMP/phpmyadmin.php?lang=en">phpMyAdmin</a></li>		
        <li>|</li>																									
		<li><a target="_blank" href="/MAMP/index.php?page=faq&amp;language=English">FAQ</a></li>
	</ul>	
</div>
</div>
</body>

